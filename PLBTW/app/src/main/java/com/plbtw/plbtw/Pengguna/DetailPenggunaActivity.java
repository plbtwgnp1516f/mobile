package com.plbtw.plbtw.Pengguna;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.plbtw.plbtw.JSONParser;
import com.plbtw.plbtw.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailPenggunaActivity extends Fragment {

    private static final String URL_CREATE="http://pinjemindong.esy.es/plbtw/pengguna/get_data_pengguna.php";
    //private static final String URL_CREATE="http://localhost/privalet_apps/user_login.php";

    private SharedPreferences sharedpreferences;
    private final String name="myAccount";
    private static final int mode = Activity.MODE_PRIVATE;
    private String username;

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    TextView lblNama,lblEmail,lblNomorTelpon,lblPoin;
    Button btnGantiPassword;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_detail_pengguna, container, false);

        sharedpreferences=getActivity().getSharedPreferences(name, mode);

        this.username=sharedpreferences.getString("username", "");

        lblNama = (TextView)v.findViewById(R.id.lblNama);
        lblEmail = (TextView)v.findViewById(R.id.lblEmail);
        lblNomorTelpon = (TextView)v.findViewById(R.id.lblNomorHandphone);
        lblPoin = (TextView)v.findViewById(R.id.lblPoin);
        new getDetailJSON(username).execute();

        btnGantiPassword= (Button) v.findViewById(R.id.btnGantiPassword);

        btnGantiPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent i = new Intent(getActivity(), GantiPasswordActivity.class);
                //startActivity(i);
            }
        });

        btnGantiPassword.setVisibility(View.GONE);

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    class getDetailJSON extends AsyncTask<String, String, String> {
        String sukses = "";
        String username="";
        String nama,email,nomortelpon,poin;

        public getDetailJSON(String username)
        {
            this.username=username;
            pDialog = new ProgressDialog(getActivity());
        }

        protected  void onPreExecute(){
            super.onPreExecute();
            pDialog.setMessage("Mengambil Data. Silahkan Tunggu...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username",username));


            try{
                JSONObject json= jParser.makeHttpRequest(URL_CREATE,"GET",params);

                //Log.d("", "-------->URL:"+URL_CREATE);
                //Log.d("", "-------->Data:"+json.toString());
                if(json!=null){
                    sukses=json.getString(TAG_PESAN);

                    if(sukses.equalsIgnoreCase("berhasil")){
                        if(json!=null){

                            Log.d("", json.toString());

                            jsonarray = json.getJSONArray(TAG_HASIL);
                            JSONObject jsonobject = jsonarray.getJSONObject(0);

                            Log.d("",jsonobject.toString());

                            nama= jsonobject.getString("nama");
                            email= jsonobject.getString("email");
                            nomortelpon= jsonobject.getString("nomor_handphone");
                            poin= jsonobject.getString("poin");
                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        //output
        protected void onPostExecute(String file_url){
            pDialog.dismiss();
            if(sukses.equalsIgnoreCase("berhasil")){
                lblNama.setText(nama);
                lblEmail.setText(email);
                lblNomorTelpon.setText(nomortelpon);
                lblPoin.setText(poin);
            }
        }
    }

}
