package com.plbtw.plbtw.Pengguna;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.plbtw.plbtw.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin on 24/05/2016.
 */
public class KurangiPoin {
    private static final String URL_CREATE="http://pinjemindong.esy.es/plbtw/pengguna/reduce_poin.php";
    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    ProgressDialog pDialog;

    public KurangiPoin(String username,String poin){
        new KurangiJumlahHadiahJSON(username,poin).execute();
    }

    class KurangiJumlahHadiahJSON extends AsyncTask<String,String,String> {
        String sukses = "";
        String username,poin;

        public KurangiJumlahHadiahJSON(String username, String poin) {
            this.username=username;
            this.poin=poin;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("poin", poin));

            try {
                JSONObject json = jParser.makeHttpRequest(URL_CREATE, "GET", params);
                if (json != null) {
                    sukses = json.getString(TAG_PESAN);

                    if (sukses.equalsIgnoreCase("berhasil")) {
                        Log.d("", json.toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

        }
    }
}
