package com.plbtw.plbtw.Hadiah;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.plbtw.plbtw.CustomAdapter.ListHadiahAdapter;
import com.plbtw.plbtw.CustomAdapter.ListRiwayatHadiahAdapter;
import com.plbtw.plbtw.JSONParser;
import com.plbtw.plbtw.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RiwayatHadiahActivity extends Fragment {

    private static final String URL_READ="http://pinjemindong.esy.es/plbtw/transaksi/read.php";
    ArrayList<HashMap<String, String>> arrayListHadiah;
    ListView listHadiah;
    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    private SharedPreferences sharedpreferences;
    private final String name = "myAccount";
    public static final int mode = Activity.MODE_PRIVATE;

    String username;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_riwayat_hadiah, container, false);
        arrayListHadiah = new ArrayList<HashMap<String, String>>();
        listHadiah = (ListView) v.findViewById(R.id.listRiwayatHadiah);

        sharedpreferences=getActivity().getSharedPreferences(name,mode);
        username=sharedpreferences.getString("username","");

        Log.d("", "---------->Username" + username);

        new listRiwayatHadiahJSON(username).execute();
        return v;
    }

    //JSON kelas untuk login
    class listRiwayatHadiahJSON extends AsyncTask<String, String, String> {
        String sukses = "",username;

        public listRiwayatHadiahJSON(String username){
            this.username=username;
            pDialog = new ProgressDialog(getActivity());
        }

        protected  void onPreExecute(){
            super.onPreExecute();
            pDialog.setMessage("Mengambil Data. Silahkan Tunggu...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));

            try{
                JSONObject json= jParser.makeHttpRequest(URL_READ,"GET",params);
                Log.d("URL : ", params.toString());

                if(json!=null){
                    sukses=json.getString(TAG_PESAN);

                    if(sukses.equalsIgnoreCase("berhasil")){
                        if(json!=null){

                            arrayListHadiah = new ArrayList<HashMap<String, String>>();
                            Log.d("Semua data: ", json.toString());

                            jsonarray = json.getJSONArray(TAG_HASIL);

                            for(int i = 0;i<jsonarray.length();i++) {
                                JSONObject jsonobject = jsonarray.getJSONObject(i);

                                String id_transaksi = jsonobject.getString("id_transaksi");
                                String nama_hadiah = jsonobject.getString("nama_hadiah");
                                String username = jsonobject.getString("username");
                                String tanggal = jsonobject.getString("tanggal");
                                String status = jsonobject.getString("status");
                                String id_hadiah = jsonobject.getString("id_hadiah");

                                //Log.d("",id_martabak+nama_martabak+harga+deskripsi+"\n");

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put("id_transaksi", id_transaksi);
                                map.put("nama_hadiah", nama_hadiah);
                                map.put("id_hadiah", id_hadiah);
                                map.put("username", username);
                                map.put("tanggal", tanggal);
                                map.put("status", status);

                                arrayListHadiah.add(map);
                            }
                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        //output
        protected void onPostExecute(String file_url){
            pDialog.dismiss();
            if(sukses.equalsIgnoreCase("berhasil")){
                ListAdapter adapter = new ListRiwayatHadiahAdapter(getActivity(),arrayListHadiah,
                        R.layout.list_riwayat_hadiah,new String[]{},
                        new int[]{});
                listHadiah.setAdapter(adapter);
            }
            else
                Toast.makeText(getActivity().getApplicationContext(), "Error :(", Toast.LENGTH_SHORT).show();

        }
    }
}
