package com.plbtw.plbtw.Hadiah;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.plbtw.plbtw.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin on 24/05/2016.
 */
public class KurangiJumlahHadiah {
    private static final String URL_CREATE="http://pinjemindong.esy.es/plbtw/hadiah/reduce_jumlah.php";
    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    ProgressDialog pDialog;

    String id_hadiah;

    public KurangiJumlahHadiah(String id_hadiah){
        this.id_hadiah=id_hadiah;
        new KurangiJumlahHadiahJSON(id_hadiah).execute();
    }

    class KurangiJumlahHadiahJSON extends AsyncTask<String,String,String> {
        String sukses = "";
        String id_hadiah;

        public KurangiJumlahHadiahJSON(String id_hadiah) {
            this.id_hadiah=id_hadiah;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_hadiah", id_hadiah));

            try {
                JSONObject json = jParser.makeHttpRequest(URL_CREATE, "GET", params);
                if (json != null) {
                    sukses = json.getString(TAG_PESAN);

                    if (sukses.equalsIgnoreCase("berhasil")) {
                        Log.d("", json.toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

        }
    }
}
