package com.plbtw.plbtw.Hadiah;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.Toast;

import com.plbtw.plbtw.JSONParser;
import com.plbtw.plbtw.MainActivity;
import com.plbtw.plbtw.Pengguna.KurangiPoin;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kevin on 24/05/2016.
 */
public class TambahTransaksi {
    String username,id_hadiah,poin;
    private Context context;

    private static final String URL_CREATE="http://pinjemindong.esy.es/plbtw/transaksi/create.php";
    private static final String URL_CHECK="http://pinjemindong.esy.es/plbtw/pengguna/check_poin.php";

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    ProgressDialog pDialog;

    public TambahTransaksi(String username, String id_hadiah,String poin, Context context){
        this.username=username;
        this.id_hadiah=id_hadiah;
        this.poin=poin;
        this.context=context;
        new CheckPoinJSON(username,poin).execute();
    }


    class CheckPoinJSON  extends AsyncTask<String,String,String> {
        String sukses = "";
        String username,poin;

        public CheckPoinJSON(String username, String poin) {
            this.username=username;
            this.poin=poin;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("poin", poin));

            try {
                JSONObject json = jParser.makeHttpRequest(URL_CHECK, "GET", params);
                if (json != null) {
                    sukses = json.getString(TAG_PESAN);

                    if (sukses.equalsIgnoreCase("berhasil")) {
                        Log.d("Semua Nama: ", json.toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if(sukses.equalsIgnoreCase("berhasil")){
                new TambahTransaksiJSON(username,id_hadiah).execute();
                new KurangiJumlahHadiah(id_hadiah);
                new KurangiPoin(username,poin);
            }
            else {
                Toast.makeText(context,"Maaf poin tidak cukup :(",Toast.LENGTH_LONG).show();
            }
        }
    }

    class TambahTransaksiJSON extends AsyncTask<String,String,String> {
        String sukses = "";
        String username,id_hadiah;

        public TambahTransaksiJSON(String username, String id_hadiah) {
            this.username=username;
            this.id_hadiah=id_hadiah;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("id_hadiah", id_hadiah));

            try {
                JSONObject json = jParser.makeHttpRequest(URL_CREATE, "GET", params);
                if (json != null) {
                    sukses = json.getString(TAG_PESAN);

                    if (sukses.equalsIgnoreCase("berhasil")) {
                        Log.d("Semua Nama: ", json.toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            if(sukses.equalsIgnoreCase("berhasil")){
                Toast.makeText(context,"Transaksi berhasil :D",Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(context,"Maaf poin anda tidak cukup",Toast.LENGTH_LONG).show();
            }
        }
    }
}
