package com.plbtw.plbtw.CustomAdapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.plbtw.plbtw.Hadiah.TambahTransaksi;
import com.plbtw.plbtw.JSONParser;
import com.plbtw.plbtw.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Kevin on 25/05/2016.
 */
public class ListRiwayatHadiahAdapter extends SimpleAdapter {

    private ArrayList<HashMap<String, String>> data;
    private Context context;
    private LayoutInflater inflater;

    TextView txtQuestion;

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    ProgressDialog pDialog;

    public ListRiwayatHadiahAdapter(Context context, ArrayList<HashMap<String, String>> data, int resource, String[] from, int[] to) {
        //super(context, data, resource, from, to);
        super(context, data, resource, from, to);
        Log.d("", "----------->Array " + data);
        this.data = data;
        this.context=context;
        //inflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }


    @Override
    public View getView(final int position, View vParentRow, ViewGroup parent) {
        View view = super.getView(position, vParentRow, parent);
        //View view=vParentRow;
        //if (view == null)
        {
            Log.d("","--------->POSITION: "+position);
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_riwayat_hadiah,parent, false);

            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            //set Text
            TextView lblNamaHadiah= (TextView) view.findViewById(R.id.lblNamaHadiah);
            TextView lblTanggal= (TextView) view.findViewById(R.id.lblTanggal);
            TextView lblIdHadiah = (TextView) view.findViewById(R.id.lblIdHadiah);
            TextView lblStatus = (TextView) view.findViewById(R.id.lblStatus);
            TextView lblIdTransaksi = (TextView) view.findViewById(R.id.lblIdTransaksi);

            String url="http://pinjemindong.esy.es/plbtw/image/"+ data.get(position).get("id_hadiah")+".png";
            Log.d("", "--------->URL" + url);
            Picasso.with(context).load(url).into(imageView);

            if(imageView.getDrawable() == null){
                imageView.setImageResource(R.drawable.no_image);
            }

            lblNamaHadiah.setText(data.get(position).get("nama_hadiah"));
            lblTanggal.setText("Tanggal: "+data.get(position).get("tanggal"));
            lblIdHadiah.setText(data.get(position).get("id_hadiah"));
            lblIdTransaksi.setText(data.get(position).get("id_transaksi"));

            lblStatus.setText(data.get(position).get("status"));
            if(lblStatus.getText().toString().equalsIgnoreCase("belum diambil"))
                lblStatus.setTextColor(Color.RED);
            else if(lblStatus.getText().toString().equalsIgnoreCase("sudah diambil"))
                lblStatus.setTextColor(Color.GREEN);

            lblIdHadiah.setVisibility(View.GONE);
            lblIdTransaksi.setVisibility(View.GONE);

        }
        return view;
    }
}
