package com.plbtw.plbtw.CustomAdapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.plbtw.plbtw.Hadiah.TambahTransaksi;
import com.plbtw.plbtw.JSONParser;
import com.plbtw.plbtw.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Kevin on 23/05/2016.
 */
public class ListHadiahAdapter extends SimpleAdapter{

    private ArrayList<HashMap<String, String>> data;
    private Context context;
    private LayoutInflater inflater;
    String username;

    TextView txtQuestion;

    private static final String URL_POIN="http://pinjemindong.esy.es/plbtw/pengguna/get_poin.php";
    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    ProgressDialog pDialog;

    public ListHadiahAdapter(Context context, ArrayList<HashMap<String, String>> data, int resource, String[] from, int[] to,String username) {
        //super(context, data, resource, from, to);
        super(context, data, resource, from, to);
        Log.d("", "----------->Array " + data);
        this.data = data;
        this.context=context;
        this.username=username;
        //inflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }


    @Override
    public View getView(final int position, View vParentRow, ViewGroup parent) {
        View view = super.getView(position, vParentRow, parent);
        //View view=vParentRow;
        //if (view == null)
        {
            Log.d("","--------->POSITION: "+position);
            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_hadiah,parent, false);

            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            //set Text
            final TextView lblNamaHadiah= (TextView) view.findViewById(R.id.lblNamaHadiah);
            TextView lblPoin= (TextView) view.findViewById(R.id.lblTanggal);
            TextView lblIdHadiah = (TextView) view.findViewById(R.id.lblIdHadiah);
            TextView lblJumlah = (TextView) view.findViewById(R.id.lblJumlah);

            String url="http://pinjemindong.esy.es/plbtw/image/"+ data.get(position).get("id_hadiah")+".png";
            Log.d("", "--------->URL" + url);
            Picasso.with(context).load(url).into(imageView);

            if(imageView.getDrawable() == null){
                imageView.setImageResource(R.drawable.no_image);
            }

            lblNamaHadiah.setText(data.get(position).get("nama_hadiah"));
            lblPoin.setText("Poin :" + data.get(position).get("poin"));
            lblIdHadiah.setText("Jumlah :" + data.get(position).get("id_hadiah"));
            lblJumlah.setText("Jumlah :" + data.get(position).get("jumlah"));

            lblIdHadiah.setVisibility(View.GONE);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtQuestion = new TextView(context);

                    String poin = data.get(position).get("poin");


                    if(data.get(position).get("jumlah").equalsIgnoreCase("0")) {
                        Toast.makeText(context, "Maaf stock habis:((", Toast.LENGTH_LONG).show();
                    }
                    else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Transaksi");
                        txtQuestion.setText("        Apakah anda setuju?");
                        builder.setView(txtQuestion);

                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new TambahTransaksi(username, data.get(position).get("id_hadiah"),data.get(position).get("poin"), context);
                            }
                        });

                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();
                    }
                }
            });
        }
        return view;
    }

}
