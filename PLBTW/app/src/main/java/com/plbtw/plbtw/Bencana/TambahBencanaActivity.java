package com.plbtw.plbtw.Bencana;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.plbtw.plbtw.JSONParser;
import com.plbtw.plbtw.R;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TambahBencanaActivity extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,LocationListener,android.location.LocationListener {
    private BetterSpinner txtJenisBencana,txtTingkatKerusakan;
    private EditText txtPenyebab,txtJumlahKorban,txtAlamat;
    private Button btnTambah;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LocationManager locManager;
    private Criteria criteria;
    private String locProvider;
    private LatLng latLng;
    private static final String[] JENIS_BENCANA = new String[] {
            "Banjir","Gempa Bumi","Puting Beliung","Kebakaran","Gunung Meletus","Pohon Tumbang","Badai","Longsor","Tsunami","Tornado","Hujan Es","Kekeringan","Petir"
    };
    private static final String[] TINGKAT_KERUSAKAN = new String[] {
            "Rendah", "Sedang", "Tinggi"
    };
    private String alamat = "", kota = "", state = "", negara = "", postalCode = "", desa = "", kecamatan= "", kabupaten= "",kelurahan="";

    private static final String URL_CREATE="http://pinjemindong.esy.es/plbtw/bencana/create.php";
    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    ProgressDialog pDialog;

    private SharedPreferences sharedpreferences;
    private final String name = "myAccount";
    public static final int mode = Activity.MODE_PRIVATE;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_tambah_bencana, container, false);
        sharedpreferences=getActivity().getSharedPreferences(name,mode);
        txtJenisBencana = (BetterSpinner)v.findViewById(R.id.txtJenisBencana);
        txtTingkatKerusakan = (BetterSpinner)v.findViewById(R.id.txtTingkatKerusakan);
        txtPenyebab = (EditText) v.findViewById(R.id.txtPenyebab);
        txtJumlahKorban = (EditText) v.findViewById(R.id.txtJumlahKorban);
        txtAlamat = (EditText) v.findViewById(R.id.txtAlamat);
        btnTambah = (Button) v.findViewById(R.id.btnTambah);
        ArrayAdapter<String> AdapterJenisBencana = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, JENIS_BENCANA);
        ArrayAdapter<String> AdapterTingkatKerusakan = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, TINGKAT_KERUSAKAN);
        txtJenisBencana.setAdapter(AdapterJenisBencana);
        txtTingkatKerusakan.setAdapter(AdapterTingkatKerusakan);

        //map
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000);

        mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
        setupGeoCurrentPosition();

        mMap.animateCamera(CameraUpdateFactory.zoomTo(18), 2000, null);
        mMap.setMyLocationEnabled(true);
        //Toast.makeText(getActivity().getApplicationContext(),"LATLANG = "+latLng.toString(),Toast.LENGTH_LONG);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                Geocoder geocoder;
                List<Address> addresses;

                try {
                    geocoder = new Geocoder(getActivity().getApplicationContext(), Locale.getDefault());
                    addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    if (addresses != null && addresses.size() > 0) {
                        alamat = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        kota = addresses.get(0).getLocality();
                        state = addresses.get(0).getAdminArea();
                        negara = addresses.get(0).getCountryName();
                        postalCode = addresses.get(0).getPostalCode();
                        desa =addresses.get(0).getLocality();
                        kecamatan=addresses.get(0).getSubAdminArea();
                        kabupaten=addresses.get(0).getAdminArea();
                        kelurahan=addresses.get(0).getSubLocality();
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title("Disini")
                        .snippet(alamat)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                txtAlamat.setText(alamat);
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkTextbox()) {
                   // new TambahBencanaJSON
                    String penyebab=txtPenyebab.getText().toString();
                    String korban=txtJumlahKorban.getText().toString();
                    String status="belum dicek";
                    String total_kerugian="";
                    String tingkat_kerusakan=txtTingkatKerusakan.getText().toString();
                    String jenis_bencana=txtJenisBencana.getText().toString();
                    String username=sharedpreferences.getString("username","");
                    new TambahBencanaJSON(username,penyebab,korban,status,total_kerugian,tingkat_kerusakan, jenis_bencana, alamat, kota, negara, desa, kelurahan, kecamatan).execute();
                    startActivity(new Intent(getActivity(), UploadImageActivity.class));
                }
            }
        });
        return v;
    }


    public boolean checkTextbox(){
        boolean temp=true;
        if(txtJenisBencana.getText().toString().isEmpty()){
            temp=false;
            txtJenisBencana.setError("tidak boleh kosong");
        }
        else{
            txtJenisBencana.setError(null);
        }
        if(txtTingkatKerusakan.getText().toString().isEmpty()){
            temp=false;
            txtTingkatKerusakan.setError("tidak boleh kosong");
        }
        else{
            txtTingkatKerusakan.setError(null);
        }
        if(txtPenyebab.getText().toString().isEmpty()){
            temp=false;
            txtPenyebab.setError("tidak boleh kosong");
        }
        else{
            txtPenyebab.setError(null);
        }
        if(txtJumlahKorban.getText().toString().isEmpty()){
            temp=false;
            txtJumlahKorban.setError("tidak boleh kosong");
        }
        else{
            txtJumlahKorban.setError(null);
        }
        if(txtAlamat.getText().toString().isEmpty()){
            temp=false;
            txtAlamat.setError("tidak boleh kosong");
        }
        else{
            txtAlamat.setError(null);
        }
        if(btnTambah.getText().toString().isEmpty()){
            temp=false;
            btnTambah.setError("tidak boleh kosong");
        }
        else{
            btnTambah.setError(null);
        }
        return temp;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.i("", "Location services connected.");
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        else {
            handleNewLocation(location);
        };
    }

    private void handleNewLocation(Location location) {
        Log.d("", location.toString());
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        latLng = new LatLng(currentLatitude, currentLongitude);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("", "Location services suspended. Please reconnect.");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), 9000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void repositionMarker(String result){
        try{
            final JSONObject json = new JSONObject(result);
            JSONArray resultArray = json.getJSONArray("results");
            JSONObject results = resultArray.getJSONObject(0);
            JSONObject geometry=results.getJSONObject("geometry");
            JSONObject location = geometry.getJSONObject("location");
            String stringLat = location.getString("lat");
            String stringLon = location.getString("lng");
            LatLng newlatlng = new LatLng(Double.parseDouble(stringLat),
                    Double.parseDouble(stringLon));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newlatlng, 15));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
            mMap.setMyLocationEnabled(true);
            mMap.clear();

            Toast.makeText(getActivity().getApplicationContext(), "Reposition",
                    Toast.LENGTH_LONG).show();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupGeoCurrentPosition(){
        int status= GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getBaseContext());
        if(status!= ConnectionResult.SUCCESS){
            int requestcode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status,getActivity(), requestcode);
            dialog.show();
            Log.d("SETUPGEO", "GAGAL");
        }
        else {
            locManager=(LocationManager) getActivity().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            criteria = new Criteria();
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            criteria.setSpeedRequired(true);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(false);
            locProvider = locManager.getBestProvider(criteria,true);
            Location currentLoc = locManager.getLastKnownLocation(locProvider);
            locManager.requestLocationUpdates(locProvider,400,1,this);
            if(currentLoc!=null) {
                //onLocationChanged(currentLoc);
                Log.d("SETUPGEO","SUKSES");
            }
            else {
                Log.d("SETUPGEO","LOKASI KOSONG BROOOOO");
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            mMap= ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

            if (mMap != null) {

            }
            else if(mMap==null){
                Toast.makeText(getActivity().getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    class TambahBencanaJSON extends AsyncTask<String,String,String> {
        String sukses = "";
        String username,penyebab,korban,status,total_kerugian,tingkat_kerusakan,jenis_bencana,
        alamat,kota,negara,desa,kelurahan,kecamatan;

        public TambahBencanaJSON(String username,String penyebab,String korban,String status,String total_kerugian,String tingkat_kerusakan,
        String jenis_bencana,String alamat,String kota,String negara,String desa,String kelurahan,String kecamatan) {
            this.username=username;
            this.penyebab=penyebab;
            this.korban=korban;
            this.status=status;
            this.total_kerugian=total_kerugian;
            this.tingkat_kerusakan=tingkat_kerusakan;
            this.jenis_bencana=jenis_bencana;
            this.alamat=alamat;
            this.kota=kota;
            this.negara=negara;
            this.desa=desa;
            this.kelurahan=kelurahan;
            this.kecamatan=kecamatan;
            pDialog = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Signing Up. Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("penyebab", penyebab));
            params.add(new BasicNameValuePair("korban", korban));
            params.add(new BasicNameValuePair("status", status));
            params.add(new BasicNameValuePair("total_kerugian", total_kerugian));
            params.add(new BasicNameValuePair("tingkat_kerusakan", tingkat_kerusakan));
            params.add(new BasicNameValuePair("jenis_bencana", jenis_bencana));
            params.add(new BasicNameValuePair("alamat", alamat));
            params.add(new BasicNameValuePair("kota", kota));
            params.add(new BasicNameValuePair("negara", negara));
            params.add(new BasicNameValuePair("desa", desa));
            params.add(new BasicNameValuePair("kelurahan", kelurahan));
            params.add(new BasicNameValuePair("kecamatan", kecamatan));

            try {
                JSONObject json = jParser.makeHttpRequest(URL_CREATE, "GET", params);
                if (json != null) {
                    sukses = json.getString(TAG_PESAN);

                    if (sukses.equalsIgnoreCase("berhasil")) {
                        Log.d("Semua Nama: ", json.toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            if(sukses.equalsIgnoreCase("berhasil")){

            }
            //startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
        }
    }


}
