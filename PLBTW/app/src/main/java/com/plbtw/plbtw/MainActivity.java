package com.plbtw.plbtw;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.plbtw.plbtw.Bencana.ListBencanaActivity;
import com.plbtw.plbtw.Bencana.RiwayatActivity;
import com.plbtw.plbtw.Hadiah.RiwayatHadiahActivity;
import com.plbtw.plbtw.Bencana.TambahBencanaActivity;
import com.plbtw.plbtw.Hadiah.ListHadiahActivity;
import com.plbtw.plbtw.Pengguna.DetailPenggunaActivity;
import com.plbtw.plbtw.Pengguna.LoginActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;

    private SharedPreferences sharedpreferences;
    private final String name = "myAccount";
    public static final int mode = Activity.MODE_PRIVATE;

    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;

    private static final String URL_CREATE="http://pinjemindong.esy.es/plbtw/pengguna/get_data_pengguna.php";

    TextView txt_username;
    TextView txt_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedpreferences=getSharedPreferences(name,mode);

        // Initializing Toolbar and setting it as the actionbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        txt_username = (TextView) navigationView.findViewById(R.id.username);
        txt_email = (TextView) navigationView.findViewById(R.id.email);

        //SET DRAWER USER PROFILE
        ImageView imageView = (ImageView) navigationView.findViewById(R.id.profile_image);
        //String url="http://10.0.3.2/martabakmobile/images/"+data.get(position).get("id_martabak")+".jpg";
        //Picasso.with(context).load(url).into(imageView);

        imageView.setImageResource(R.drawable.logo);
        new getDetailJSON(sharedpreferences.getString("username", "")).execute();
        //email.setText("admin@martabakmobile.com");
        //username.setText("admin");

        ListBencanaActivity listBencanaActivity = new ListBencanaActivity();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, listBencanaActivity);
        fragmentTransaction.commit();

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                //Closing drawer on item click
                drawerLayout.closeDrawers();

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;

                    case R.id.nav_listbencana:
                        ListBencanaActivity listBencanaActivity = new ListBencanaActivity();
                        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.frame, listBencanaActivity);
                        fragmentTransaction.commit();
                        return true;

                    case R.id.nav_tambahbencana:
                        TambahBencanaActivity tambahBencanaActivity = new TambahBencanaActivity();
                        android.support.v4.app.FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction2.replace(R.id.frame, tambahBencanaActivity);
                        fragmentTransaction2.commit();
                        return true;

                    case R.id.nav_riwayat:
                        RiwayatActivity riwayatActivity = new RiwayatActivity();
                        android.support.v4.app.FragmentTransaction fragmentTransaction4 = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction4.replace(R.id.frame, riwayatActivity);
                        fragmentTransaction4.commit();
                        //Toast.makeText(getApplicationContext(), "Drafts Selected", Toast.LENGTH_SHORT).show();
                        return true;

                    case R.id.nav_hadiah:
                        ListHadiahActivity listHadiahActivity = new ListHadiahActivity();
                        android.support.v4.app.FragmentTransaction fragmentTransaction5 = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction5.replace(R.id.frame, listHadiahActivity);
                        fragmentTransaction5.commit();
                        //Toast.makeText(getApplicationContext(), "Drafts Selected", Toast.LENGTH_SHORT).show();
                        return true;

                    case R.id.nav_riwayat_hadiah:
                        RiwayatHadiahActivity riwayatHadiahActivity = new RiwayatHadiahActivity();
                        android.support.v4.app.FragmentTransaction fragmentTransaction6 = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction6.replace(R.id.frame, riwayatHadiahActivity);
                        fragmentTransaction6.commit();
                        //Toast.makeText(getApplicationContext(), "Drafts Selected", Toast.LENGTH_SHORT).show();
                        return true;


                    case R.id.nav_pengguna:
                        //Toast.makeText(getApplicationContext(), "Drafts Selected", Toast.LENGTH_SHORT).show();
                        DetailPenggunaActivity detailPenggunaActivity = new DetailPenggunaActivity();
                        android.support.v4.app.FragmentTransaction fragmentTransaction7 = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction7.replace(R.id.frame, detailPenggunaActivity);
                        fragmentTransaction7.commit();
                        return true;
                    case R.id.nav_keluar:

                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.remove("username");
                        editor.remove("password");
                        editor.commit();

                        finish();
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(i);

                        return true;
                    default:
                        //Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                        return true;

                }
            }
        });

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar, R.string.openDrawer, R.string.closeDrawer){

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class getDetailJSON extends AsyncTask<String, String, String> {
        String sukses = "";
        String username="";
        String nama,email,nomortelpon,poin;

        public getDetailJSON(String username)
        {
            this.username=username;
        }

        protected  void onPreExecute(){
            super.onPreExecute();
        }

        protected  String doInBackground(String... args){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username",username));

            try{
                JSONObject json= jParser.makeHttpRequest(URL_CREATE,"GET",params);

                //Log.d("", "-------->URL:"+URL_CREATE);
                //Log.d("", "-------->Data:"+json.toString());
                if(json!=null){
                    sukses=json.getString(TAG_PESAN);

                    if(sukses.equalsIgnoreCase("berhasil")){
                        if(json!=null){

                            Log.d("", json.toString());

                            jsonarray = json.getJSONArray(TAG_HASIL);
                            JSONObject jsonobject = jsonarray.getJSONObject(0);

                            Log.d("",jsonobject.toString());

                            nama= jsonobject.getString("nama");
                            email= jsonobject.getString("email");
                            nomortelpon= jsonobject.getString("nomor_handphone");
                            poin= jsonobject.getString("poin");
                        }
                    }
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        //output
        protected void onPostExecute(String file_url){

            if(sukses.equalsIgnoreCase("berhasil")){
                txt_username.setText(nama);
                txt_email.setText("Poin :"+poin);
            }
        }
    }
}
