package com.plbtw.plbtw.CustomAdapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.plbtw.plbtw.R;

/**
 * Created by Kevin on 24/05/2016.
 */
public class CustomAlertDialog extends Dialog {

    protected CustomAlertDialog(Context context) {
        super(context);

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.custom_alert_dialog, null);
        this.setContentView(dialoglayout);
    }

}
