package com.plbtw.plbtw.Pengguna;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.plbtw.plbtw.JSONParser;
import com.plbtw.plbtw.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends AppCompatActivity {


    Button btnDaftar;
    EditText txtUsername,txtPassword,txtRePassword,txtNama,txtEmail,txtNomorTelpon;
    //ReCaptcha reCaptcha;

    private static final String URL_CREATE="http://pinjemindong.esy.es/plbtw/pengguna/create.php";
    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        txtUsername= (EditText) findViewById(R.id.txtUsername);
        txtPassword= (EditText) findViewById(R.id.txtPassword);
        txtRePassword= (EditText) findViewById(R.id.txtRePassword);
        txtNama= (EditText) findViewById(R.id.txtNama);
        txtEmail= (EditText) findViewById(R.id.txtEmail);
        txtNomorTelpon= (EditText) findViewById(R.id.txtNomorTelpon);
        btnDaftar = (Button) findViewById(R.id.btnDaftar);
        //ReCaptcha reCaptcha = (ReCaptcha)findViewById(R.id.recaptcha);
        //reCaptcha.showChallengeAsync("6Lcrox8TAAAAAD0f1Nzs3xuLl3MntLWuB9se_wH4", this);
    }

    public void btnDaftar_onClick(View v){
        if(checkTextbox()){
            new RegisterJSON(txtUsername.getText().toString(), txtPassword.getText().toString(), txtNama.getText().toString(),
                    txtEmail.getText().toString(),txtNomorTelpon.getText().toString()).execute();
        }
    }

    public boolean checkTextbox(){
        boolean temp=true;
        if(txtUsername.getText().toString().isEmpty()){
            txtUsername.setError("username tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtUsername.setError(null);
        }
        if(txtPassword.getText().toString().isEmpty()){
            txtPassword.setError("password tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtPassword.setError(null);
        }
        if(txtRePassword.getText().toString().isEmpty()){
            txtRePassword.setError("re-password tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtRePassword.setError(null);
        }
        if(txtNama.getText().toString().isEmpty()){
            txtNama.setError("nama tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtNama.setError(null);
        }
        if(txtEmail.getText().toString().isEmpty()){
            txtEmail.setError("email tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtEmail.setError(null);
        }
        if(txtNomorTelpon.getText().toString().isEmpty()){
            txtNomorTelpon.setError("nomor telpon tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtNomorTelpon.setError(null);
        }
        return temp;
    }

    class RegisterJSON extends AsyncTask<String,String,String> {
        String sukses = "";
        String username,password,nama,email,nomor_handphone;
        public RegisterJSON(String username, String password, String nama, String email,String nomor_handphone) {
            this.username=username;
            this.password=password;
            this.nama=nama;
            this.email=email;
            this.nomor_handphone=nomor_handphone;
            pDialog = new ProgressDialog(RegisterActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Signing Up. Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("password", password));
            params.add(new BasicNameValuePair("nama", nama));
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("nomor_handphone", nomor_handphone));

            try {
                JSONObject json = jParser.makeHttpRequest(URL_CREATE, "GET", params);
                if (json != null) {
                    sukses = json.getString(TAG_PESAN);

                    if (sukses.equalsIgnoreCase("berhasil")) {
                        Log.d("Semua Nama: ", json.toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            if(sukses.equalsIgnoreCase("berhasil")){
                Toast.makeText(getApplicationContext(), "Akun berhasil dibuat", Toast.LENGTH_LONG).show();
                finish();
                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Akun gagal dibuat",Toast.LENGTH_LONG).show();
            }
        }
    }
}
