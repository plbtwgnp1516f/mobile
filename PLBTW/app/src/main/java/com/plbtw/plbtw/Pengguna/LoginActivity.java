package com.plbtw.plbtw.Pengguna;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.plbtw.plbtw.JSONParser;
import com.plbtw.plbtw.MainActivity;
import com.plbtw.plbtw.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    Button btnLogin,btnDaftar;
    EditText txtUsername,txtPassword;

    private SharedPreferences sharedpreferences;
    private final String name = "myAccount";
    public static final int mode = Activity.MODE_PRIVATE;

    private static final String URL_CREATE="http://pinjemindong.esy.es/plbtw/pengguna/read.php";
    private static final String TAG_PESAN="message";
    private static final String TAG_HASIL="result";

    JSONParser jParser = new JSONParser();
    JSONArray jsonarray = null;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedpreferences=getSharedPreferences(name,mode);

        if(sharedpreferences!=null) {
            if(!sharedpreferences.getString("username","").isEmpty()){
                finish();
                Intent i = new Intent(this,MainActivity.class);
                startActivity(i);
            }
        }

        btnDaftar=(Button) findViewById(R.id.btnDaftar);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtUsername = (EditText) findViewById(R.id.txtUsername);
    }

    public boolean checkTextbox(){
        boolean temp=true;
        if(txtUsername.getText().toString().isEmpty()){
            txtUsername.setError("username tidak boleh kosong :(");
            temp=false;
        }
        else {
            txtUsername.setError(null);
        }
        if(txtPassword.getText().toString().isEmpty()){
            txtPassword.setError("password tidak boleh kosong :(");
            temp=false;
        }
        else{
            txtPassword.setError(null);
        }

        return temp;
    }

    public void btnLogin_onClick(View v){
        if(checkTextbox()) {
            new LoginJSON(txtUsername.getText().toString(),txtPassword.getText().toString()).execute();
        }
    }

    public void btnDaftar_onClick(View v){
        finish();
        Intent i = new Intent(this,RegisterActivity.class);
        startActivity(i);
    }

    class LoginJSON extends AsyncTask<String,String,String> {
        String sukses = "";
        String username,password;
        public LoginJSON(String username, String password) {
            this.username=username;
            this.password=password;
            pDialog = new ProgressDialog(LoginActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Signing Up. Please Wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("username", username));
            params.add(new BasicNameValuePair("password", password));

            try {
                JSONObject json = jParser.makeHttpRequest(URL_CREATE, "POST", params);
                if (json != null) {
                    sukses = json.getString(TAG_PESAN);

                    if (sukses.equalsIgnoreCase("berhasil")) {
                        Log.d("Semua Nama: ", json.toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            if(sukses.equalsIgnoreCase("berhasil")){
                Toast.makeText(getApplicationContext(),"Berhasil Login",Toast.LENGTH_LONG).show();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("username",txtUsername.getText().toString());
                editor.putString("password",txtUsername.getText().toString());
                editor.apply();
                finish();
                Intent i = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(i);
                Log.d("", "-------------->berhasil login");
            }
            else {
                Toast.makeText(getApplicationContext(),"Gagal Login",Toast.LENGTH_LONG).show();
                Log.d("", "-------------->gagal login");
            }
        }
    }
}
